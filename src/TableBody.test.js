import React from 'react';
import ReactDOM from 'react-dom';
import { shallow, mount } from 'enzyme';
import Row from './Row';
import TableBody from './TableBody';

describe('<TableBody />', () => {
  it('should exist', () => {
    expect(TableBody).toBeDefined();
  });

  it('should render Rows for each item in props.data', () => {
    // SETUP

    const rows = [
      { id: 1, name: 'AAA', tags: ['a', 'b', 'c'] },
      { id: 2, name: 'BBB', tags: ['e', 'f', 'g'] },
      { id: 3, name: 'CCC', tags: ['h', 'j', 'k'] }
    ];

    const onEditFunction = jest.fn();
    const onDeleteFunction = jest.fn();

    // RENDER / ACT

    const element = shallow(<TableBody data={rows} onEditRow={onEditFunction} onDelete={onDeleteFunction}/>);

    // VERIFY

    expect(element.find(Row).length).toEqual(rows.length);
  });

  it('should pass onEditRow and onDelete functions to each child Row', () => {
    // SETUP

    const rows = [
      { id: 1, name: 'AAA', tags: ['a', 'b', 'c'] },
      { id: 2, name: 'BBB', tags: ['e', 'f', 'g'] },
      { id: 3, name: 'CCC', tags: ['h', 'j', 'k'] }
    ];

    const onEditFunction = jest.fn();
    const onDeleteFunction = jest.fn();

    // RENDER + ACT

    const element = shallow(<TableBody data={rows} onEditRow={onEditFunction} onDelete={onDeleteFunction}/>);

    // const firstRow = element.find(Row).first().dive();
    //
    // firstRow.find('.editItem button').simulate('click', 1);
    // firstRow.find('.deleteItem button').simulate('click', 1);
    //
    // // VERIFY
    //
    // expect(onEditFunction).toHaveBeenCalledWith(rows[0]);
    // expect(onDeleteFunction).toHaveBeenCalledWith(rows[0]);

    const expectedRow = <Row key={rows[0].id} itemData={rows[0]} onDelete={onDeleteFunction} onEdit={onEditFunction} />;

    expect(element.contains(expectedRow)).toEqual(true);
  });
});
