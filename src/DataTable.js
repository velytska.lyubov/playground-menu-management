import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './DataTable.css';

import TableBody from './TableBody';
import Toolbar from './Toolbar';
import ItemEditor from './ItemEditor';

class DataTable extends Component {
  static propTypes = {
    dataProvider: PropTypes.object.isRequired,
  };

  constructor (props) {
    super(props);

    this.props.dataProvider.register(data => {
      this.setState({ data, filteredData: data });
    });

    this.state = {
      data: Array.from(props.dataProvider.get()),
      filteredData: Array.from(props.dataProvider.get()),
      isEditing: false,
      currentItem: null
    };

    this.filterByTag = this.filterByTag.bind(this);
    this.filterByName = this.filterByName.bind(this);
    this.onStartEditing = this.onStartEditing.bind(this);
    this.onCancelEditing = this.onCancelEditing.bind(this);
    this.onEditorFinished = this.onEditorFinished.bind(this);
    this.onDelete = this.onDelete.bind(this);
  }

  filterByTag (filter) {
    const filteredData =
      this.state.data
        .filter(item => item.tags.indexOf(filter) >= 0);

    this.setState({ filteredData });
  }

  filterByName (filter) {
    const filteredData =
      this.state.data
        .filter(item => item.name.toLowerCase().indexOf(filter.toLowerCase()) >= 0);

    this.setState({ filteredData });
  }

  onStartEditing (item = null) {
    this.setState({ isEditing: true, currentItem: item });
  }

  onCancelEditing () {
    this.setState({ isEditing: false, currentItem: null });
  }

  onEditorFinished (data) {
    const isNewItem = this.state.currentItem === null;

    this.setState({ isEditing: false, currentItem: null }, () => {
      if (isNewItem) {
        this.props.dataProvider.create(data);
      }
      else {
        this.props.dataProvider.update(data);
      }
    });
  }

  onDelete (data) {
    if (window.confirm('are you sure you want to delete this item?')) {
      this.props.dataProvider.delete(data);
    }
  }

  renderItemEditor () {
    if (this.state.isEditing) {
      return (
        <ItemEditor onSubmit={this.onEditorFinished}
                    onCancel={this.onCancelEditing}
                    item={this.state.currentItem}/>);
    }
    else {
      return null;
    }
  }

  render () {
    return (
      <div className="dataTable">
        <Toolbar onCreateNew={this.onStartEditing}
                 onFilterByTag={this.filterByTag}
                 onFilterByName={this.filterByName}/>
        {this.renderItemEditor()}
        <TableBody data={this.state.filteredData}
                   onEditRow={this.onStartEditing}
                   onDelete={this.onDelete}
        />
      </div>
    );
  }
}

export default DataTable;
