import uuid from 'uuid/v4';
import DataProvider from  './DataProvider';

class MockDataProvider extends DataProvider {
  constructor (source) {
    super();

    this.data = Array.from(source);
  }

  get () {
    return this.data;
  }

  findById (id) {
    const result = this.data.filter(item => item.id === id);

    return result.length === 0 ? null : result[0];
  }

  update (item) {
    const idx = this.data.findIndex(x => x.id === item.id);

    if (idx >= 0) {
      this.data[idx] = item;
    }

    super.notify();
  }

  create (item) {
    const id = uuid();

    this.data.push({ ...item, id });

    super.notify();
  }

  delete (item) {
    this.data = this.data.filter(x => x.id !== item.id);

    super.notify();
  }
}

export default MockDataProvider;
