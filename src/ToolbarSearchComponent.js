import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import 'react-select/dist/react-select.css';

const FILTERS = [
  { value: 'nameFilter', label: 'name' },
  { value: 'tagFilter', label: 'tag' }
];

class ToolbarSearchComponent extends Component {
  static propTypes = {
    onFilterByName: PropTypes.func,
    onFilterByTag: PropTypes.func,
  };

  constructor (props) {
    super(props);

    this.state = {
      filterBy: 'nameFilter',
      nameFilter: '',
      tagFilter: '',
      selectedOption: ''
    };

    this.handleFilterByName = this.handleFilterByName.bind(this);
    this.handleFilterByTag = this.handleFilterByTag.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleFilterByTag (event) {
    this.setState({ tagFilter: event.target.value }, () => {
      this.props.onFilterByTag(this.state.tagFilter);
    });
  }

  handleFilterByName (event) {
    this.setState({ nameFilter: event.target.value }, () => {
      this.props.onFilterByName(this.state.nameFilter);
    });
  }

  handleChange (selectedOption) {
    this.setState({ filterBy: selectedOption });
  };

  render () {
    return (
      <div className="ToolbarSearchComponent">
        <label>Filter by:</label>
        <Select options={FILTERS}
                value={this.state.filterBy}
                onChange={this.handleChange}
                simpleValue={true}
        />
        {
          this.state.filterBy === 'nameFilter' &&
          <input name="nameFilter"
                 type="text"
                 value={this.state.nameFilter}
                 onChange={this.handleFilterByName}/>
        }

        {
          this.state.filterBy === 'tagFilter' &&
          <input name="tagFilter"
                 type="text"
                 value={this.state.tagFilter}
                 onChange={this.handleFilterByTag}/>
        }
      </div>
    );
  }
}

export default ToolbarSearchComponent;
