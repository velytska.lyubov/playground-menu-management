import React from 'react';
import './App.css';

import DataTable from './DataTable';
import LocalStorageDataProvider from './LocalStorageDataProvider';

const dataProvider = new LocalStorageDataProvider(window.localStorage);

const App = () => (

  <div className="app">
    <h3 className="titleApp">Eat at Home App</h3>

    <DataTable dataProvider={dataProvider}/>

  </div>
);

export default App;
