import React from 'react';
import PropTypes from 'prop-types';
import './Row.css';

const Row = ({ itemData, onEdit, onDelete, onToggleRow, isActive }) => (
 <div className="dishItemContainer"> <div className="dishItem">
    <div className="openClose">
      <button onClick={() => onToggleRow(itemData.id)}>{ isActive ? "Close" : "Open" }</button>
    </div>
    <div className="dishItemName">{itemData.name}</div>

    <div className="dishItemTags">{itemData.tags}</div>
    <div className="editItem">
      <button onClick={() => onEdit(itemData)}>Edit</button>
    </div>
    <div className="deleteItem">
      <button onClick={() => onDelete(itemData)}>Delete</button>
    </div>
 </div>
   { isActive && <div className="dishItemDescription">{itemData.description}</div> }
 </div>

);

Row.propTypes = {
  itemData: PropTypes.object.isRequired,
  onEdit: PropTypes.func,
  onDelete: PropTypes.func,
  onToggleRow: PropTypes.func,
  isActive: PropTypes.bool,
};

export default Row;
