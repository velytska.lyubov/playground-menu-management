class DataProvider {
  constructor () {
    this.listeners = [];
    this.data = [];
  }

  get () {
  }

  findById () {
  }

  update (item) {
  }

  create (item) {
  }

  delete (item) {
  }

  register (callback) {
    this.listeners.push(callback);
  }

  notify () {
    this.listeners.forEach(callback => callback(this.data));
  }
}

export default DataProvider;
