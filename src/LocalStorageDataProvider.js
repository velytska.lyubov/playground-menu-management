import uuid from 'uuid/v4';
import DataProvider from  './DataProvider';

class LocalStorageDataProvider extends DataProvider {
  constructor (storage) {
    super();

    this.storage = storage;

    if (this.storage.getItem('dishes') === null) {
      this.storage.setItem('dishes', JSON.stringify([]));
    }

    this.data = JSON.parse(this.storage.getItem('dishes'));
  }

  get () {
    return this.data;
  }

  findById (id) {
    const result = this.data.filter(item => item.id === id);

    return result.length === 0 ? null : result[0];
  }

  update (item) {
    const idx = this.data.findIndex(x => x.id === item.id);

    if (idx >= 0) {
      this.data[idx] = item;
    }

    this.storage.setItem('dishes', JSON.stringify(this.data));

    super.notify();
  }

  create (item) {
    const id = uuid();

    this.data.push({ ...item, id });

    this.storage.setItem('dishes', JSON.stringify(this.data));

    super.notify();
  }

  delete (item) {
    this.data = this.data.filter(x => x.id !== item.id);

    this.storage.setItem('dishes', JSON.stringify(this.data));

    super.notify();
  }
}

export default LocalStorageDataProvider;
