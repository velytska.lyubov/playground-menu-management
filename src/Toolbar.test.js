import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import Toolbar from './Toolbar';

describe('<Toolbar />', () => {
  it('should exist', () => {
    expect(Toolbar).toBeDefined();
  });

  it('should invoke props.onCreateNew when "Create New" button is clicked', () => {
    const item = null;
    const onCreateNew = jest.fn();
    const element = shallow(<Toolbar onCreateNew={onCreateNew}/>);

    element.find('.buttonCreateNew button').last().simulate('click', 1);

    expect(onCreateNew).toHaveBeenCalledWith(item);
  });

  it('should invoke props.onFilterByName with value of state.nameFilter when corresponding input changes', () => {

    const onFilterByName = jest.fn();

  });

  it('should invoke props.onFilterByTag with value of state.tagFilter when corresponding input changes');
});
