import React from 'react';
import ReactDOM from 'react-dom';
import ItemEditor from './ItemEditor';

describe('<ItemEditor />', () => {
  it('should exist', () => {
    expect(ItemEditor).toBeDefined();
  });

  it('should set initial state to match props.item fields when props.item is not null');

  it('should set initial state to default empty values when props.item is null or missing');

  it('should keep state.tags array that corresponds to state.tagsString');

  it('should invoke props.onSubmit when the Save button is clicked');
});

