import MockDataProvider from './MockDataProvider';
import uuid from 'uuid/v4';

describe('MockDataProvider', () => {
  const mockData = [
    { id: uuid(), name: 'A', value: 1 },
    { id: uuid(), name: 'B', value: 2 },
    { id: uuid(), name: 'C', value: 3 },
    { id: uuid(), name: 'D', value: 4 },
    { id: uuid(), name: 'E', value: 5 },
  ];

  let dataProvider;

  beforeEach(() => {
    dataProvider = new MockDataProvider(mockData);
  });

  it('should return all initial items when .get() is called', () => {
    expect(dataProvider.get().length).toBe(mockData.length);
  });

  it('should return correct item when .findById() is called with corresponding ID', () => {
    expect(dataProvider.findById(mockData[2].id)).toMatchObject(mockData[2]);
  });

  it('should return null when .findById() is called with unknown ID', () => {
    expect(dataProvider.findById(1)).toBeNull();
  });

  it('should create new item when .create() is called', () => {
    dataProvider.create({ name: 'F', value: 6 });

    expect(dataProvider.get().length).toBe(mockData.length + 1);
  });

  it('should modify corresponding item when .update() is called with item that has a matching ID', () => {
    const newItem = { name: 'F', value: 6 };

    dataProvider.create(newItem);

    const id = dataProvider.get().filter(item => item.name === newItem.name)[0].id;

    dataProvider.update({ id, name: 'F', value: 10 });

    expect(dataProvider.findById(id).value).toBe(10);
  });

  it('should ignore .update() call when no current item has a matching ID', () => {
    const newItem = { name: 'F', value: 6 };

    dataProvider.create(newItem);

    const id = dataProvider.get().filter(item => item.name === newItem.name)[0].id;

    dataProvider.update({ id: 111, name: 'F', value: 10 });

    expect(dataProvider.findById(id).value).toBe(6);
  });
});
