import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './ItemEditor.css';
import Select from 'react-select';
import 'react-select/dist/react-select.css';

const TAGS = [
  { value: 'breakfast', label: 'Breakfast' },
  { value: 'lunch', label: 'Lunch' },
  { value: 'dinner', label: 'Dinner' },
  { value: 'supper', label: 'Supper' },
  { value: 'snack', label: 'Snack' }
];

class ItemEditor extends Component {
  static propTypes = {
    item: PropTypes.object,
    onSubmit: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
  };

  static defaultProps = {
    item: null,
  };

  constructor (props) {
    super(props);

    const item = this.props.item || { id: null, name: '', tags: [] };

    this.state = {
      id: item.id,
      name: item.name,
      description: item.description,
      tags: item.tags,
      selectedOption: []
    };

    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange (selectedOption) {
    this.setState({ tags: selectedOption });
  };

  handleNameChange (event) {
    this.setState({ name: event.target.value.toLowerCase() });
  }

  handleDescriptionChange (event) {
    this.setState({ description: event.target.value.toLowerCase() });
  }

  handleSubmit (event) {
    event.preventDefault();

    const { id, name, description, tags } = this.state;

    this.props.onSubmit({ id, name, description, tags });
  }

  render () {
    return (
      <form className="itemEditor" onSubmit={this.handleSubmit}>
        <label>Name of dish:
          <input type="text" value={this.state.name} onChange={this.handleNameChange} required/>
        </label>
        <lable>Description:
          <input type="text" value={this.state.description} onChange={this.handleDescriptionChange}/>
        </lable>
        <div className="tags">
          <label>Choose the Tag:
            <Select
              name="form-field-name"
              placeholder='Choose Tag'
              onChange={this.handleChange}
              options={TAGS}
              simpleValue={true}
              value={this.state.tags}
              multi={true}
            />
          </label>
        </div>
        <input type="submit" value="Save"/>

        <input type="button" value="Cancel" onClick={this.props.onCancel}/>
      </form>
    );
  }
}

export default ItemEditor;
