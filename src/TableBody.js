import React, { Component } from 'react';
import PropTypes from 'prop-types';
import './TableBody.css';

import Row from './Row';


class TableBody extends Component {
  static propTypes = {
    data: PropTypes.array.isRequired,
    onEditRow: PropTypes.func,
    onDelete: PropTypes.func
  };

  constructor (props) {
    super(props);

    this.state = {
      activeRowId: null
    };

    this.onToggleRow = this.onToggleRow.bind(this);
  }

  onToggleRow (id) {
    this.setState({ activeRowId: this.state.activeRowId === id ? null : id })
  }

  render () {
    const { data, onEditRow, onDelete } = this.props;

    return (
      <div className="dataTableBody">
        {data.map(item => <Row key={item.id}
                               itemData={item}
                               isActive={this.state.activeRowId === item.id}
                               onToggleRow={this.onToggleRow}
                               onEdit={onEditRow}
                               onDelete={onDelete}/>)}
      </div>
    );
  }
}

export default TableBody;
