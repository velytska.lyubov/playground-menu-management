import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import DataTable from './DataTable';
import ItemEditor from './ItemEditor';
import Toolbar from './Toolbar';
import MockDataProvider from './MockDataProvider';
import TableBody from './TableBody';
import Row from './Row';

describe('<DataTable/>', () => {
  it('should exist', () => {
    expect(DataTable).toBeDefined();
  });

  it('should not have ItemEditor by default', () => {
    // setup
    const mockDataProvider = new MockDataProvider([]);

    // execute / render
    const element = shallow(<DataTable dataProvider={mockDataProvider}/>);

    // verify
    expect(element.find(ItemEditor).length).toBe(0);
  });

  it('should show ItemEditor with default empty values when Create New is clicked in Toolbar', () => {
    // setup
    const mockDataProvider = new MockDataProvider([]);

    // execute / render
    const element = shallow(<DataTable dataProvider={mockDataProvider}/>);
    element.find(Toolbar).dive().find('.buttonCreateNew button').simulate('click', 1);
    element.update();

    // verify
    expect(element.find(ItemEditor).length).toBe(1);
    expect(element.find(ItemEditor).prop('item')).toBe(null);
  });

  it('should show ItemEditor with corresponding item when Edit is clicked in one of the rows', () => {
    // setup
    const data = [
      { id: 1, name: 'AAA', tags: ['a', 'b', 'c'] },
      { id: 2, name: 'BBB', tags: ['e', 'f', 'g'] },
      { id: 3, name: 'CCC', tags: ['h', 'j', 'k'] }
    ];

    const mockDataProvider = new MockDataProvider(data);

    const rowIdx = 1;

    // execute / render

    const element = shallow(<DataTable dataProvider={mockDataProvider}/>);
    const tableBody = element.find(TableBody).dive();
    const row = tableBody.find(Row).at(rowIdx).dive();

    row.find('.editItem button').simulate('click', 1);

    element.update();

    // verify
    expect(element.find(ItemEditor).length).toBe(1);
    expect(element.find(ItemEditor).prop('item')).toBe(data[rowIdx]);
  });

  // TODO place delete confirmation in Row ???
  xit('should display confirmation dialog when Delete is called in one of the items');

  it('should update state.filteredData when filterByTag is invoked', () => {
    const data = [
      { id: 1, name: 'AAA', tags: ['a', 'b', 'c'] },
      { id: 2, name: 'BBB', tags: ['e', 'f', 'g'] },
      { id: 3, name: 'CCC', tags: ['a', 'j', 'k'] }
    ];

    const mockDataProvider = new MockDataProvider(data);

    const dataTable = shallow(<DataTable dataProvider={mockDataProvider}/>);

    expect(dataTable.find(TableBody).dive().find(Row).length).toEqual(3);

    const toolbar = dataTable.find(Toolbar).dive();
    const tagInput = toolbar.find('.searchByTag input');
    tagInput.simulate('change', { target: { value: 'a' } });
    dataTable.update();

    expect(dataTable.find(TableBody).dive().find(Row).length).toEqual(2);
  });

  it('should update state.filteredData when filterByName is invoked', () => {
    const data = [
      { id: 1, name: 'AAA', tags: ['a', 'b', 'c'] },
      { id: 2, name: 'BBB', tags: ['e', 'f', 'g'] },
      { id: 3, name: 'CCC', tags: ['a', 'j', 'k'] }
    ];

    const mockDataProvider = new MockDataProvider(data);

    const dataTable = shallow(<DataTable dataProvider={mockDataProvider}/>);

    expect(dataTable.find(TableBody).dive().find(Row).length).toEqual(3);

    const toolbar = dataTable.find(Toolbar).dive();
    const nameInput = toolbar.find('.searchByName input');
    nameInput.simulate('change', { target: { value: 'aa' } });
    dataTable.update();

    expect(dataTable.find(TableBody).dive().find(Row).length).toEqual(1);
  });
});
