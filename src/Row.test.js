import React from 'react';
import ReactDOM from 'react-dom';
import { shallow } from 'enzyme';
import Row from './Row';

describe('<Row />', () => {
  it('should exist', () => {
    expect(Row).toBeDefined();
  });

  it('should render item tags as comma-separated string', () => {
    const item = { id: 1, name: 'AAA', tags: ['a', 'b', 'c'] };
    const element = shallow(<Row itemData={item} />);
    const tagsDiv = <div className="dishItemTags">a, b, c</div>;

    expect(element.contains(tagsDiv)).toEqual(true);
  });

  it('should invoke props.onEdit when Edit button is clicked', () => {
    const item = { id: 1, name: 'AAA', tags: ['a', 'b', 'c'] };
    const onEditFunction = jest.fn();
    const element = shallow(<Row itemData={item} onEdit={onEditFunction} />);

    element.find('.editItem button').last().simulate('click', 1);

    expect(onEditFunction).toHaveBeenCalledWith(item);
  });

  it('should invoke props.onDelete when Delete button is clicked', () => {
    const item = { id: 1, name: 'AAA', tags: ['a', 'b', 'c'] };
    const onDeleteFunction = jest.fn();
    const element = shallow(<Row itemData={item} onDelete={onDeleteFunction} />);

    element.find('.deleteItem button').last().simulate('click', 1);

    expect(onDeleteFunction).toHaveBeenCalledWith(item);
  });

  it('should invoke props.onToggleRow when Open button is clicked', () => {
    const item = { id: 1, name: 'AAA', tags: ['a', 'b', 'c'] };
    const onToggleRowFunction = jest.fn();
    const element = shallow(<Row itemData={item} onToggleRow={onToggleRowFunction} />);

    element.find('.openClose button').last().simulate('click', 1);

    expect(onToggleRowFunction).toHaveBeenCalledWith(item.id);
  });
});
