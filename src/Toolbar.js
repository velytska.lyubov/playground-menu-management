import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Select from 'react-select';
import 'react-select/dist/react-select.css';
import './Toolbar.css';
import ToolbarSearchComponent from './ToolbarSearchComponent';

class Toolbar extends Component {
  static propTypes = {
    onCreateNew: PropTypes.func,
    onFilterByTag: PropTypes.func,
    onFilterByName: PropTypes.func,
  };

  constructor (props) {
    super(props);

    this.state = {
      showComponent: false,
    };
  }

  render () {
    const { onCreateNew, onFilterByTag, onFilterByName } = this.props;

    return (
      <div className="dataTableToolbar">
        <div className="buttonCreateNew">
          <button onClick={() => onCreateNew(null)}>Create New</button>
        </div>

        <ToolbarSearchComponent onFilterByTag={onFilterByTag} onFilterByName={onFilterByName} />
      </div>
    );
  }
}

export default Toolbar;
