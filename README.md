# Recipes

## Topics to learn

- [ ] React foundations
- [ ] CRUD
    * UI (validations, tables)
    * data storage
- [ ] TDD (test driven development) Unit tests
    * Test coverage
- [ ] Basic Node js Server
- [ ] Deploy web app
- [ ] Basics of Agile / Kanban

## Target app feature

* Simple table with the listof dishes
* Ampty table/ Table with no data
* Navigation/ Toolbar
* Editor: Create / Edit items
* Search
* Filter (breakfast, lunch, dinner, snack...)
* Data Storage

